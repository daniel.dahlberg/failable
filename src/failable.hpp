#pragma once

template <class T, typename E = int>
class Failable
{
public:
    Failable(const T &in) : value{in}, valid{true} {};
    const bool is_valid() const { return valid; };

    static const Failable<T> fromError(E i)
    {
        return Failable<T>{}.setError(i);
    };

    T &&unwrap() { return std::move(value); }
    T &&unwrap_or(void f(E err))
    {
        if (!valid)
        {
            f(error);
        }
        else
            return std::move(value);
    }

private:
    Failable() : valid{false} {};

    Failable<T> &setError(E i)
    {
        valid = false;
        error = i;
        return *this;
    };

    union {
        T value;
        E error;
    };

    bool valid;
};
