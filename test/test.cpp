#include <gtest/gtest.h>
#include "failable.hpp"

auto maybe_failing_func(bool fail, int value = 1) -> Failable<int>
{
    if (fail)
    {
        return Failable<int>::fromError(1);
    }
    else
    {
        return value;
    }
}

TEST(Failable, CreateFailing)
{
    const auto failable = maybe_failing_func(true);
    ASSERT_TRUE(!failable.is_valid());
}

TEST(Failable, CreateNotFailing)
{
    auto failable = maybe_failing_func(false, 15);
    ASSERT_TRUE(failable.is_valid());
    const auto unwrapped = failable.unwrap();
    ASSERT_EQ(unwrapped, 15);
}

TEST(Failable, unwrap_failed_object)
{
    auto failable = maybe_failing_func(true);

    failable.unwrap_or([](auto e) { std::cout << "Failed with err " << e << std::endl; });
}
