
MAKE:=make 


all: source test

source:
	$(MAKE) -C src all

test: source
	$(MAKE) -C test test

clean:
	$(MAKE) -C test clean
